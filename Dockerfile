FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /config/requirements/requirements.txt /config/
#RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /config/requirements.txt
RUN mkdir /src
WORKDIR /src
# EXPOSE 8000
# CMD ["gunicorn", "core.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "3", "--log-level=debug"]