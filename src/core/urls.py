
"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view
from rest_framework_jwt.views import (
    obtain_jwt_token, 
    refresh_jwt_token, 
    verify_jwt_token
)

admin.site.site_header = 'Shipay'
admin.site.site_title = 'Shipay'

if settings.DEBUG:
    schema_view = get_swagger_view(title='Shipay')

urlpatterns = [
    # Authentication
    re_path(r'^authentication/token/refresh/', refresh_jwt_token),
    re_path(r'^authentication/token/verify/', verify_jwt_token),
    re_path(r'^authentication/token/', obtain_jwt_token),
    
    # Loading swagger interface
    # re_path(r'^$', schema_view),

    path('admin/', admin.site.urls, name="admin"),
    url('(?P<version>[v1|v2]+)/', include('pagamento.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)