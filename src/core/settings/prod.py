from .base import *
import os

# Tempo de duracao do token(um mes)
JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=2592000)
}

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': os.getenv('DB_HOST'),
        'PORT': os.getenv('DB_PORT')
    }
}

# Static files (AWS S3)
# https://simpleisbetterthancomplex.com/tutorial/2017/08/01/how-to-setup-amazon-s3-in-a-django-project.html
# AWS_ACCESS_KEY_ID = 'key'
# AWS_SECRET_ACCESS_KEY = 'key'
# AWS_STORAGE_BUCKET_NAME = 'name'
# AWS_S3_CUSTOM_DOMAIN = 'domain'
# AWS_S3_OBJECT_PARAMETERS = {
#     'CacheControl': 'max-age=86400',
# }
# AWS_LOCATION = 'static'

# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'staticfiles'),
# ]

# STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
# STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
# AWS_S3_REGION_NAME = 'us-east-2' #change to your region
# AWS_S3_SIGNATURE_VERSION = 's3v4'

# AWS_PUBLIC_MEDIA_LOCATION = 'media/public'
# DEFAULT_FILE_STORAGE = 'core.storage_backends.PublicMediaStorage'

# AWS_PRIVATE_MEDIA_LOCATION = 'media/private'
# PRIVATE_FILE_STORAGE = 'core.storage_backends.PrivateMediaStorage'

# logger
# boto3_session = Session(
#   aws_access_key_id=AWS_ACCESS_KEY_ID,
#   aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
#   region_name=AWS_S3_REGION_NAME
# )

# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'simple': {
#             'format': u"%(asctime)s [%(levelname)-8s] %(message)s",
#             'datefmt': "%Y-%m-%d %H:%M:%S"
#         },
#         'aws': {
#             'format': u"%(asctime)s [%(levelname)-8s] %(message)s",
#             'datefmt': "%Y-%m-%d %H:%M:%S"
#         },
#     },
#     'handlers': {
#         'console': {
#             'class': 'logging.StreamHandler',
#             'formatter': 'simple',
#         },
#         'debug_views': {
#             'level': 'INFO',
#             'class': 'watchtower.CloudWatchLogHandler',
#             'boto3_session': boto3_session,
#             'log_group': 'Views',
#             'stream_name': 'DjangoInfoViews',
#             'formatter': 'aws',
#         },
#         'debug_models': {
#             'level': 'INFO',
#             'class': 'watchtower.CloudWatchLogHandler',
#             'boto3_session': boto3_session,
#             'log_group': 'Models',
#             'stream_name': 'DjangoInfoModels',
#             'formatter': 'aws',
#         },
#         'debug_serializers': {
#             'level': 'INFO',
#             'class': 'watchtower.CloudWatchLogHandler',
#             'boto3_session': boto3_session,
#             'log_group': 'Serializers',
#             'stream_name': 'DjangoInfoSerializers',
#             'formatter': 'aws',
#         },
#         'debug_celery': {
#             'level': 'INFO',
#             'class': 'watchtower.CloudWatchLogHandler',
#             'boto3_session': boto3_session,
#             'log_group': 'Celery',
#             'stream_name': 'DjangoInfoCelery',
#             'formatter': 'aws',
#         },
#     },
#     'loggers': {
#         'debug_views': {
#             'level': 'INFO',
#             'handlers': ['debug_views'],
#             'propagate': True,
#         },
#         'debug_models': {
#             'level': 'INFO',
#             'handlers': ['debug_models'],
#             'propagate': False,
#         },
#         'debug_serializers': {
#             'level': 'INFO',
#             'handlers': ['debug_serializers'],
#             'propagate': False,
#         },
#         'debug_celery': {
#             'level': 'INFO',
#             'handlers': ['debug_celery'],
#             'propagate': False,
#         }
#     }
# }
