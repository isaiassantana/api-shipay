from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from .serializers import *
from .models import *
from rest_framework import viewsets
from django.http import HttpResponse, Http404


class PagamentoViewSet(viewsets.ViewSet):
    
    
    def get_serializer(self):
        return PagamentoCreateSerializer()


    def get_object(self, cnpj):
        try:
            return Estabelecimento.objects.get(cnpj=cnpj, dt_deletado=None)
        except Estabelecimento.DoesNotExist:
            raise Http404

    
    def list(self, request, *args, **kwargs):
        '''
            Retornando pagamentos.
        '''
        instance = self.get_object(request.GET.get('cnpj'))
        serializer = PagamentoListSerializer(instance)
        return Response(serializer.data)
    
    
    def create(self, request, *args, **kwargs):
        '''
            Criando uma transação.
        '''
        serializer = PagamentoCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"aceito": True}, status=status.HTTP_201_CREATED)


class EstabelecimentoViewSet(viewsets.ViewSet):
    
    
    def get_serializer(self):
        return EstabelecimentoCreateSerializer()

    
    def create(self, request, *args, **kwargs):
        '''
            Criando uma estabelecimento.
        '''
        serializer = EstabelecimentoCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
