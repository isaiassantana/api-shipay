from django.db import models

# Create your models here.
class Estabelecimento(models.Model):
    nome = models.CharField("Nome", max_length=30)
    cnpj = models.CharField("Cnpj", max_length=18)
    dono = models.CharField("Dono", max_length=30)
    telefone = models.CharField("Telefone", max_length=15)
    
    nm_usuario_cri = models.CharField("Usuário que criou", max_length=60)
    nm_usuario_edi = models.CharField("Usuário que editou", max_length=60, null=True, blank=True)
    nm_usuario_del = models.CharField("Usuário que deletou", max_length=60, null=True, blank=True)
    ie_permissao_vis = models.BooleanField("Visualização precisa de senha?", default=False)
    ie_permissao_edi = models.BooleanField("Edição precisa de senha?", default=False)
    ie_permissao_del = models.BooleanField("Exclusão precisa de senha?", default=False)
    dt_criado = models.DateTimeField("Criado em", auto_now_add=True)
    dt_atualizado = models.DateTimeField("Atualizado em", auto_now=False, null=True, blank=True)
    dt_deletado = models.DateTimeField("Deletado em", auto_now=False, null=True, blank=True)
     
    class Meta:
        db_table = "estabelecimento"
        verbose_name = 'Estabelecimento'
        verbose_name_plural = 'Estabelecimentos'
        ordering = ('-dt_criado',)

    
    def __str__(self):
        return self.nome


class Pagamento(models.Model):
    estabelecimento = models.ForeignKey(Estabelecimento, on_delete=models.SET_NULL, null=True, blank=True)
    cliente = models.CharField("Cliente", max_length=11)
    valor = models.FloatField()
    descricao = models.CharField("Descrição", max_length=150)
    
    nm_usuario_cri = models.CharField("Usuário que criou", max_length=60)
    nm_usuario_edi = models.CharField("Usuário que editou", max_length=60, null=True, blank=True)
    nm_usuario_del = models.CharField("Usuário que deletou", max_length=60, null=True, blank=True)
    ie_permissao_vis = models.BooleanField("Visualização precisa de senha?", default=False)
    ie_permissao_edi = models.BooleanField("Edição precisa de senha?", default=False)
    ie_permissao_del = models.BooleanField("Exclusão precisa de senha?", default=False)
    dt_criado = models.DateTimeField("Criado em", auto_now_add=True)
    dt_atualizado = models.DateTimeField("Atualizado em", auto_now=False, null=True, blank=True)
    dt_deletado = models.DateTimeField("Deletado em", auto_now=False, null=True, blank=True)
     
    class Meta:
        db_table = "pagamento"
        verbose_name = 'Pagamento'
        verbose_name_plural = 'Pagamentos'
        ordering = ('-dt_criado',)

    
    def __str__(self):
        return "{} - {}".format(self.estabelecimento, self.cliente)