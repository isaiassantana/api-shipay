from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.test import TestCase
from django.contrib.auth import get_user_model


class BaseTests(TestCase):


    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
        'tdd@gmail',
        'senha12345'
        )
        self.client.force_authenticate(self.user)


class EstabelecimentoTests(BaseTests):


    def test_criando_estabelecimento(self):
        url = 'http://127.0.0.1:8000/v1/estabelecimentos'
        data = {
            "nome": "Ifood",
            "cnpj": "67732287000115",
            "dono": "Joaquim",
            "telefone": "32145678"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class PagamentoTests(BaseTests):
    

    def test_criando_pagamento(self):
        url = 'http://127.0.0.1:8000/v1/estabelecimentos'
        data = {
            "nome": "Ifood",
            "cnpj": "67732287000115",
            "dono": "Joaquim",
            "telefone": "32145678"
        }
        response = self.client.post(url, data, format='json')
        
        url = 'http://127.0.0.1:8000/v1/transacoes'
        data = {
            "estabelecimento": "67732287000115",
            "cliente": "19090729070",
            "valor": 9.90,
            "descricao": "Teste"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_listando_pagamento(self):
        url = 'http://127.0.0.1:8000/v1/estabelecimentos'
        data = {
            "nome": "Ifood",
            "cnpj": "67732287000115",
            "dono": "Joaquim",
            "telefone": "32145678"
        }
        response = self.client.post(url, data, format='json')

        url = 'http://127.0.0.1:8000/v1/transacoes/estabelecimento?cnpj=67732287000115'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    
    def test_listando_pagamento_estabelecimento_nao_existe(self):
        url = 'http://127.0.0.1:8000/v1/transacoes/estabelecimento?cnpj=67732287000115'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_pagamento_estabelecimento_nao_existe(self):
        url = 'http://127.0.0.1:8000/v1/transacoes'
        data = {
            "estabelecimento": "61864087000130",
            "cliente": "19090729070",
            "valor": 9.90,
            "descricao": "Teste"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_pagamento_cpf_invalido(self):
        url = 'http://127.0.0.1:8000/v1/transacoes'
        data = {
            "estabelecimento": "61864087000130",
            "cliente": "19090729070",
            "valor": 9.90,
            "descricao": "Teste"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        

    def test_pagamento_cnpj_invalido(self):
        """
        Ensure we can create a new account object.
        """
        url = 'http://127.0.0.1:8000/v1/transacoes'
        data = {
            "estabelecimento": "61864087000131",
            "cliente": "19090729070",
            "valor": 9.90,
            "descricao": "Teste"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)