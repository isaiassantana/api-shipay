# encondig= UTF-8
# !bin/env/python3

from rest_framework_jwt.settings import api_settings
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from rest_framework.validators import *
from rest_framework import serializers
from django.db import transaction
from rest_framework import status
from django.db.models import F
from django.db.models import Q
from .models import *
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from validate_docbr import CPF, CNPJ


class PagamentoBaseListSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Pagamento
		fields = (
			'cliente',
			'valor',
			'descricao'
		)


class EstabelecimentoListSerializer(serializers.ModelSerializer):
 
	class Meta:
		model = Estabelecimento
		fields = (
			'nome',
			'cnpj',
			'dono',
			'telefone'
		)


class PagamentoListSerializer(serializers.Serializer):
	estabelecimento = serializers.SerializerMethodField()
	recebimentos = serializers.SerializerMethodField()
	
	class Meta:
		fields = (
			'estabelecimento',
			'recebimentos'
		)

	def get_estabelecimento(self, obj):
		serializer = EstabelecimentoListSerializer(obj)
		return serializer.data


	def get_recebimentos(self, obj):
		pagamentos = Pagamento.objects.filter(estabelecimento__cnpj=obj.cnpj)
		serializer = PagamentoBaseListSerializer(pagamentos, many=True)
		return serializer.data


class PagamentoCreateSerializer(serializers.Serializer):
	estabelecimento = serializers.CharField(allow_null=False, allow_blank=False, required=True, write_only=True)
	cliente = serializers.CharField(allow_null=False, allow_blank=False, required=True, write_only=True)
	descricao = serializers.CharField(allow_null=True, allow_blank=True, required=True, write_only=True)
	valor = serializers.FloatField(allow_null=False, required=True, write_only=True)


	def validate(self, data):
		cpf = CPF()
		cnpj = CNPJ()
		
		if Estabelecimento.objects.filter(cnpj=data.get('estabelecimento')).count() == 0:
			raise serializers.ValidationError({"aceito": False})

		if cnpj.validate(data.get('estabelecimento')) is False:
			raise serializers.ValidationError({"aceito": False})

		if cpf.validate(data.get('cliente')) is False:
			raise serializers.ValidationError({"aceito": False})

		return data

	
	@transaction.atomic
	def create(self, validated_data):
		pagamento = Pagamento(
			estabelecimento = Estabelecimento.objects.get(cnpj=validated_data.get('estabelecimento')),
			cliente = validated_data.get('cliente'),
			valor = validated_data.get('valor'),
			descricao = validated_data.get('descricao')
		)
		pagamento.save()
		return pagamento


class EstabelecimentoCreateSerializer(serializers.ModelSerializer):
	

	class Meta:
		model = Estabelecimento
		fields = (
			'id',
			'nome',
			'cnpj',
			'dono',
			'telefone'
		)
