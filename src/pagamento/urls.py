# encondig= UTF-8
# !bin/env/python3

from django.conf.urls import url, include
from django.urls import path
from .views import *


urlpatterns = [
	path('transacoes/estabelecimento', PagamentoViewSet.as_view({'get': 'list'})),
	path('transacoes', PagamentoViewSet.as_view({'post': 'create'})),

	path('estabelecimentos', EstabelecimentoViewSet.as_view({'post': 'create'}))
]