.SILENT:

clean:
	find . \( -name *.py[co] -o -name __pycache__ \) -delete

up:
	docker-compose.yml up

stop-all:
	docker stop $(docker ps -aq)

test:
	docker exec -it api_shipay python manage.py test --settings=core.settings.local

makemigrations:
	docker exec -it api_shipay python manage.py makemigrations --settings=core.settings.local

migrate:
	docker exec -it api_shipay python manage.py migrate --settings=core.settings.local

createsuperuser:
	docker exec -it api_shipay python manage.py createsuperuser --settings=core.settings.local

collectstatic:
	docker exec -it api_shipay python manage.py createsuperuser --settings=core.settings.local
